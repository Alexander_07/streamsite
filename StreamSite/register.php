<?php
    require_once("includes/config.php");
    require_once("classes/FormSanitizer.php");
    require_once("classes/Account.php");
    require_once("classes/Constants.php");


    $account = new Account($conn);

    if (isset($_POST["submitButton"])){
        $firstName = FormSanitizer::sanitizeString($_POST['firstName']);
        $lastName = FormSanitizer::sanitizeString($_POST['lastName']);
        $username = FormSanitizer::sanitizeUsernameOrEmail($_POST['username']);
        $email = FormSanitizer::sanitizeUsernameOrEmail($_POST['email']);
        $email2 = FormSanitizer::sanitizeUsernameOrEmail($_POST['email2']);
        $password = FormSanitizer::sanitizePassword($_POST['password']);
        $password2 = FormSanitizer::sanitizePassword($_POST['password2']);

       // echo $firstName . " " . $lastName . " " . $username . " " . $email . " " . $email2 . " " . $password .  " " . $password2;

        $resutlt = $account->register($firstName, $lastName, $username, $email, $email2, $password, $password2);

        if ($resutlt){
            header("Location: index.php");
        }
    }

    function getInputValue($name){
        if (isset($_POST[$name])){
            echo $_POST[$name];
        }
    }
    

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Регистрация</title>
    <link rel="stylesheet" href="assets\css\style.css">
</head>
<body>
    <div class="signInContainer">
        <div class="column">
            <div class="header">
                <img src="assets\img\logo.png" alt="Logo">
                <h3>Регистрация</h3>
                <span>Добро пожаловать!</span>
            </div>

          

            <form action="" method="POST">
                <?php echo $account->getError(Constants::$firstNameAndLastNameCharacters); ?>
                <input type="text" name="firstName" placeholder="Имя" value="<?php getInputValue("firstName"); ?>" required>

                <?php echo $account->getError(Constants::$firstNameAndLastNameCharacters); ?>
                <input type="text" name="lastName"  placeholder="Фамилия" >

                <?php echo $account->getError(Constants::$userNameCharacters); ?>
                <?php echo $account->getError(Constants::$userNameExist); ?>
                <input type="text" name="username" placeholder="Логин" >

                <?php echo $account->getError(Constants::$emailDontMatch); ?>
                <?php echo $account->getError(Constants::$emailExist); ?>
                <input type="email" name="email" placeholder="Email" >
                <input type="email" name="email2" placeholder="Подтвердите Email" >

                <?php echo $account->getError(Constants::$passwordDontMatch); ?>
                <?php echo $account->getError(Constants::$passwordLength); ?>
                <input type="password" name="password" placeholder="Пароль" >
                <input type="password" name="password2" placeholder="Подтвердите пароль" >

                <input type="submit" name="submitButton" value="Зарегистрироваться">
            </form>
            <a href="login.php" class="signIn">Уже есть аккаунт</a>
        </div>
    </div>
</body>
</html>