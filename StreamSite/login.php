<?php
    require_once("includes/config.php");
    require_once("classes/FormSanitizer.php");
    require_once("classes/Account.php");
    require_once("classes/Constants.php");
    

    $account = new Account($conn);

    if (isset($_POST["submitButton"])){
        $username = FormSanitizer::sanitizeUsernameOrEmail($_POST['username']);
        $password = FormSanitizer::sanitizePassword($_POST['password']);

        $resutlt = $account->login($username, $password);

        if ($resutlt){
           $_SESSION['userLoggedIn'] = $username;
           header("Location: index.php");
        }
      
    }

    function getInputValue($name){
        if (isset($_POST[$name])){
            echo $_POST[$name];
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Войти</title>
    <link rel="stylesheet" href="assets\css\style.css">
</head>
<body>
    <div class="signInContainer">
        <div class="column">
            <div class="header">
                <img src="assets\img\logo.png" alt="Logo">
                <h3>Войти</h3>
                <span>Добро пожаловать!</span>
            </div>

            <form action="" method="POST">
                <?php echo $account->getError(Constants::$loginError); ?>
                <input type="text" name="username" placeholder="Логин" value="<?php getInputValue(username); ?>" required>
                <input type="password" name="password" placeholder="Пароль" required>
                <input type="submit" name="submitButton" value="Войти">
            </form>
            <a href="register.php" class="signIn">Еще нет аккаунта</a>
        </div>
    </div>
</body>
</html>